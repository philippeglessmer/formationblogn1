<?php

namespace App\Form\Article;

use App\Entity\Article\Article;
use App\Entity\Article\ArticleCategorie;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => '',
                    'placeholder' => 'Titre de l\'article'
                ],
            ])
            ->add('contenu')
            ->add('etat', ChoiceType::class, [
                'choices'  => ['Brouillion' => 0, 'Publier' => 1],
                'attr' => [

                ],
                'placeholder' => ' -- Choisissez --'
            ])
            ->add('images', FileType::class, [
                'mapped' => false,
                'required' => false,
            ])
//            ->add('images', CollectionType::class, [
//                'entry_type' => FileType::class,
//                'required' => false,
//                'label' => false,
//                'mapped' => false,
//                'allow_add' => true,
//                'allow_delete' => true,
//                'by_reference' => false,
//            ])
            ->add('categories', EntityType::class, [
                'class' => ArticleCategorie::class,
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.libelle', 'ASC');
                },
                'choice_label' => 'libelle',
                'placeholder' => '-- choisissez --'
            ]);
//            ->add('tags')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
