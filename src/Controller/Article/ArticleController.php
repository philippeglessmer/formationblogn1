<?php

namespace App\Controller\Article;

use App\Entity\Article\Article;
use App\Entity\Article\ArticleIllustration;
use App\Form\Article\ArticleType;
use App\Repository\Article\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/article')]
class ArticleController extends AbstractController
{
    #[Route('/', name: 'app_article_article_index', methods: ['GET'])]
    public function index(ArticleRepository $articleRepository): Response
    {
        return $this->render('article/article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_article_article_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ArticleRepository $articleRepository, SluggerInterface $slugger): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $illustrationFile = $form->get('images')->getData();
            if($illustrationFile){
                $originalFilename = pathinfo($illustrationFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$illustrationFile->guessExtension();

                try {
                    $illustrationFile->move(
                        $this->getParameter('illustration_blog'),
                        $newFilename
                    );
                    $illustration = new ArticleIllustration();
                    $illustration->setFichier($newFilename);
                    $article->addIllustration($illustration);
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

            }

            $articleRepository->save($article, true);

            return $this->redirectToRoute('app_article_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/article/new.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_article_article_show', methods: ['GET'])]
    public function show(Article $article): Response
    {
        return $this->render('article/article/show.html.twig', [
            'article' => $article,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_article_article_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Article $article, ArticleRepository $articleRepository,  SluggerInterface $slugger): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $illustrationFile = $form->get('images')->getData();
            if($illustrationFile) {
                $originalFilename = pathinfo($illustrationFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $illustrationFile->guessExtension();

                try {
                    $illustrationFile->move(
                        $this->getParameter('illustration_blog'),
                        $newFilename
                    );
                    $illustration = new ArticleIllustration();
                    $illustration->setFichier($newFilename);
                    $article->addIllustration($illustration);
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
            $articleRepository->save($article, true);



            return $this->redirectToRoute('app_article_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/article/edit.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_article_article_delete', methods: ['POST'])]
    public function delete(Request $request, Article $article, ArticleRepository $articleRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $articleRepository->remove($article, true);
        }

        return $this->redirectToRoute('app_article_article_index', [], Response::HTTP_SEE_OTHER);
    }
}
